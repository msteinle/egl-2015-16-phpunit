<?php
/**
 * Created by PhpStorm.
 * User: mirko
 * Date: 23.05.16
 * Time: 22:14
 */

require_once('librairie.php');

class RechercheTest extends PHPUnit_Framework_TestCase {

    public function testRecherche_premier()
    {
        $listeTest = [1, 2, 3]; // une liste de test
        $resultat = recherche(1, $listeTest);
        $this->assertEquals(true, $resultat);
    }

}
 