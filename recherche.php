<!DOCTYPE html>
<html>
<head lang="fr">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>Recherche</title>
    <!-- Bootstrap CSS compiled and minified CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Bootstrap theme (optional) -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <h1>La liste</h1>
    <ul>
    <?php
    require_once('librairie.php');
    foreach($LISTE as $nombre)
        echo "<li>$nombre</li>";
    ?>
    </ul>
    <form class="form-inline" role="form" action="recherche.php" method="post">
        <div class="form-group">
            <label for="val">Valeur à chercher</label>
            <input type="text" placeholder="valeur" class="form-control" id="val" name="valeurCherchee" required>
        </div>
        <button type="submit" class="btn btn-default" name="recherche">Cherche mon vieux!</button>
    </form>

    <?php

        if (isset($_POST['recherche'])) {
            $valeur = $_POST['valeurCherchee'];
            if (recherche($valeur, $LISTE)) {
                echo "La valeur '$valeur' est présente dans la liste!";
            } else {
                echo "La valeur '$valeur' n'est pas présente!";
            }
        }
    ?>
</div>
</body>
</html>